(function () {
    "use strict";
    var audio = new Audio();
    var songTitle = document.getElementById("songTitle");
    var dir = "./audio/ROYALTY FREE MUSIC by BENSOUND/";
    var ext = ".mp3";
    var plyListArr = ["A New Beginning",
        "Acoustic Breeze",
        "Cute",
        "Going Higher",
        "Happiness",
        "India",
        "Jazz Comedy",
        "Jazzy Frenchy",
        "Little Planet",
        "Love",
        "Psychedelic",
        "Relaxing",
        "Romantic",
        "Sunny",
        "Tenderness",
        "The Jazz Piano",
        "The Lounge",
        "Ukulele"];
    var plyListIndex = 0;
    var prevPlydSongIndex = 0;
    var songSrc = dir + plyListArr[0] + ext;
    var pauseSvgPath = "M18 64 L18 0 L0 0 L0 64 M28 64 L46 64 L46 0 L28 0 Z";
    var playSvgPath = "M0 64 L54 32 L0 0 Z";


    songTitle.innerText = plyListArr[plyListIndex];


    // OBJECT REFERENCES
    var playBtn = document.getElementById("playPauseBtn");
    var progressBar = document.getElementById("progressBar");
    var playBtnPath = document.getElementById("path");
    var cover = document.getElementById("coverView");
    var playlistView = document.getElementById("playlistView");
    var playlistBtn = document.getElementById("playlistBtn");
    var playlistBtnIcon = document.getElementById("playlistBtnIcon");
    var currentTimeText = document.getElementById("currTime");
    var durationTimeText = document.getElementById("durTime");
    var forwardBtn = document.getElementById("forwardBtn");
    var backwardBtn = document.getElementById("backwardBtn");

    // INITIALIZE AND LOAD THE SONG
    function initPlayer(path) {
        // Reset the UI
        progressBar.value = "0.0";
        currentTimeText.innerText = "00:00";
        durationTimeText.innerText = "00:00";
        // Init player
        audio.loop = false;
        audio.autoplay = false;
        audio.preload = "metadata";
        audio.src = path;
    }

    // TOGGLE BETWEEN PLAY & PAUSE SVG PATHS
    function togglePlayPauseBtn() {
        if (audio.paused) {
            audio.play();
            playBtnPath.setAttribute('d', pauseSvgPath);
        } else {
            audio.pause();
            playBtnPath.setAttribute('d', playSvgPath);
        }
    }

    // TOGGLE BETWEEN "ALBUM COVER" AND "PLAYLIST" VIEW
    function toggleCoverPlaylist() {
        if (playlistView.style.display == "none") {
            cover.style.display = "none";
            playlistView.style.display = "block";
        }
        else {
            cover.style.display = "block";
            playlistView.style.display = "none";
        }
    }

    // SEEKING
    function seek(e) {
        var slctdPosition = e.offsetX / progressBar.offsetWidth; // %
        audio.currentTime = slctdPosition * audio.duration;
    }

    // UPDATE THE PROGRESS BAR
    function updateProgressBar() {
        if (audio.readyState == 0) {
            // WE DO NOTHING. In webkit/blink based browsers (e.g. Chrome) metadata is loaded after the media
            // so is not available while JS is running --> audio.duration = NaN.
        } else {
            progressBar.value = (audio.currentTime / audio.duration);
        }
    }

    // CALCULATE AND DISPLAY "CURRENT TIME/SONG LENGTH" values
    function songTimeTextUpdate() {
        if (audio.readyState == 0) {
            // WE DO NOTHING. In webkit/blink based browsers (e.g. Chrome) metadata is loaded after the media
            // so is not available while JS is running --> audio.duration = NaN.
        } else {
            var currentMins = Math.floor(audio.currentTime / 60);
            var currentSecs = Math.floor(audio.currentTime - currentMins * 60);
            var durationMins = Math.floor(audio.duration / 60);
            var durationSecs = Math.floor(audio.duration - durationMins * 60);

            if (currentSecs < 10) {
                currentSecs = "0" + currentSecs;
            }
            if (durationSecs < 10) {
                durationSecs = "0" + durationSecs;
            }
            if (currentMins < 10) {
                currentMins = "0" + currentMins;
            }
            if (durationMins < 10) {
                durationMins = "0" + durationMins;
            }

            currentTimeText.innerText = currentMins + ":" + currentSecs;
            durationTimeText.innerText = durationMins + ":" + durationSecs;
        }
    }

    // GENERATE PLAYLIST (ul li)
    function generatePlaylist(arr, parentElem, ulID) {
        // Generate 'ul' element and set the 'id' attribute
        var ulistElem = document.createElement("ul");
        ulistElem.setAttribute("id", ulID);

        // Generate 'li' elements
        for (var i in arr) {
            var elem = document.createElement("li");
            var songIndex = (parseInt(i) + 1);

            // Indent song names from 1-9, with whitespace
            if (songIndex < 10) {
                elem.innerText = " " + songIndex + ". " + arr[i];
            } else if (songIndex >= 10) {

                elem.innerText = songIndex + ". " + arr[i];
            }

            ulistElem.appendChild(elem);
            parentElem.appendChild(ulistElem);
        }
    }

    generatePlaylist(plyListArr, document.getElementById("playlistView"), "playlist");

    // FORWARD BUTTON
    function switchToNextTrack() {
        // If the last track of the playlist is at the top of the cue, return to the first track of the playlist
        if (plyListIndex == (plyListArr.length - 1)) {
            plyListIndex = 0;
        } else {
            plyListIndex++;
        }
        songSrc = dir + plyListArr[plyListIndex] + ext;
        songTitle.innerText = plyListArr[plyListIndex];
        playBtnPath.setAttribute('d', pauseSvgPath);
    }

    // BACKWARD BUTTON
    function switchToPrevTrack() {
        // If the first track of the playlist is at the top of the cue, go to the last track of the playlist
        if (plyListIndex == 0) {
            plyListIndex = plyListArr.length - 1;
        } else {
            plyListIndex--;
        }
        songSrc = dir + plyListArr[plyListIndex] + ext;
        songTitle.innerText = plyListArr[plyListIndex];
        playBtnPath.setAttribute('d', pauseSvgPath);
    }

    var playlist = document.getElementById("playlist");

    // Create an HTMLCollection[] from li elements
    var liArr = playlist.getElementsByTagName("li");

    // Convert the liArr HTMLCollection[] to real Array
    var liArrArray = [].slice.call(liArr); // Conversation might not work under IE9

    // SET STYLE OF THE ACTIVE SONG NAME ON PLAYLIST VIEW
    function setActiveSongStyle() {
        if (prevPlydSongIndex != plyListIndex) {
            liArr[prevPlydSongIndex].removeAttribute("id");
            prevPlydSongIndex = plyListIndex;
        }
        liArr[plyListIndex].setAttribute("id", "active");
    }

    // SWITCH TO THE SELECTED TRACK OF THE PLAYLIST
    function selectTrackFromPlaylist(event) {
        var target = event.target;
        var indexOfSelectedSong = liArrArray.indexOf(target);
        // If (indexOfSelectedSong == -1) the user clicks too fast, we do nothing
        if (indexOfSelectedSong != -1) {
            plyListIndex = indexOfSelectedSong;
            songTitle.innerText = plyListArr[indexOfSelectedSong];
            playBtnPath.setAttribute('d', pauseSvgPath);
            songSrc = dir + plyListArr[indexOfSelectedSong] + ext;
            initPlayer(songSrc);
            setActiveSongStyle();
            audio.play();
        }
    }

    // EVENT HANDLING
    playBtn.addEventListener("click", togglePlayPauseBtn);
    progressBar.addEventListener("click", seek);
    playlistBtn.addEventListener("click", toggleCoverPlaylist);
    playlist.addEventListener("click", selectTrackFromPlaylist);
    backwardBtn.addEventListener("click", function () {
        switchToPrevTrack();
        initPlayer(songSrc);
        audio.play();
        setActiveSongStyle();
    });

    forwardBtn.addEventListener("click", function () {
        switchToNextTrack();
        initPlayer(songSrc);
        audio.play();
        setActiveSongStyle();
    });

    audio.addEventListener("ended", function () {
        switchToNextTrack();
        initPlayer(songSrc);
        audio.play();
        setActiveSongStyle();
    });

    audio.addEventListener("timeupdate", function () {
        updateProgressBar();
        songTimeTextUpdate();
    });


    initPlayer(songSrc);
    setActiveSongStyle();
})();