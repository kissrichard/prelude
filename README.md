# Prelude
<p align="center">
Prelude is a lightweight, browser based audio player <b>without external libraries or jQuery</b>   

![](Prelude.png)
</p>
